import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutocompleteInputComponent } from './components/autocomplete-input/autocomplete-input.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { PasswordInputComponent } from './components/password-input/password-input.component';
import { LoginComponent } from './pages/login/login.component';
import { PagenotfoundComponent } from './pages/pagenotfound/pagenotfound.component';
import { ModulesComponent } from './pages/modules/modules.component';
import { TableComponent } from './pages/table/table.component';
import { ConfigSiteComponent } from './pages/config-site/config-site.component';
import { LoginService } from './services/login.service';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    AutocompleteInputComponent,
    SearchbarComponent,
    NavbarComponent,
    LoginFormComponent,
    PasswordInputComponent,
    LoginComponent,
    PagenotfoundComponent,
    ModulesComponent,
    TableComponent,
    ConfigSiteComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
