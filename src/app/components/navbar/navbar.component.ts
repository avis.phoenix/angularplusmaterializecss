import { Component } from '@angular/core';
import { UserDisplay } from 'src/app/interfaces/user.interface';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';

declare var M;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  public optionsTest = {
    Apple: null,
    Microsoft: null,
    Google: null
  };

  private sideNavMaterial: any;
  private sideCollapsMaterial: any;
  private userBtnMaterial: any;

  public isLogin: boolean;
  public theUser: UserDisplay;

  constructor(private authServ: LoginService,
    private router: Router) {
    authServ.userLogin.subscribe((user) => {
      if (user != null && user != undefined) {
        this.theUser = {
          rol: '',
          language: '',
          lastLogin: new Date(),
          username: ''
        }
        this.theUser.rol = user.rol;
        this.theUser.language = user.language;
        this.theUser.lastLogin = user.lastLogin;
        this.theUser.username = user.username;
      }
      this.isLogin = user != null && user != undefined;
      setTimeout(()=>{
        this.verifyUI();
      },500);
    })
  }

  verifyUI() {
    if (this.isLogin){
      if (!this.sideNavMaterial){
        this.sideNavMaterial = M.Sidenav.init(document.getElementById('mobile-menu'));
      }
      if (!this.sideCollapsMaterial) {
        this.sideCollapsMaterial = M.Collapsible.init(document.getElementById('sidecollaps'));
      }
  
      if (!this.userBtnMaterial) {
        this.userBtnMaterial = M.Dropdown.init(document.getElementById('userbtn'), { constrainWidth: false })
      }
    } else {
      if (this.sideNavMaterial){
        this.sideNavMaterial.destroy();
        this.sideNavMaterial=null;
      }
      if (this.sideCollapsMaterial) {
        this.sideCollapsMaterial.destroy();
        this.sideCollapsMaterial = null;
      }
  
      if (this.userBtnMaterial) {
        this.userBtnMaterial.destroy();
        this.userBtnMaterial = null;
      }
    }
  }

  toggleMenu() {
    if (this.sideNavMaterial.isOpen) {
      this.sideNavMaterial.close();
    } else {
      this.sideNavMaterial.open();
    }
  }

  logout(){
    this.authServ.logout();
    this.router.navigate(['/login']);
  }

}
