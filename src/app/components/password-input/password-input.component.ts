import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-password-input',
  templateUrl: './password-input.component.html',
  styleUrls: ['./password-input.component.css']
})
export class PasswordInputComponent implements OnInit {
  @Input() options : any;
  @Input() set disabled (value : boolean) {
    this.isDisabled = value;
    if (this.showPass && value){
      this.toggleShowPassword();
    }
  }
  @Output() passwordChange: EventEmitter<string> = new EventEmitter<string>();
  @Input() set password(value: string) {

    if (value !== this.realPass) {
      if (value == null || value == undefined) {
        value = '';
      }
      this.realPass = value;
      this.passwordView = value;
      if (!this.showPass) {
        let len = value.length;
        setTimeout(() => {
          this.passwordView = '•'.repeat(len);
        }, this.delayCopy);
      }

    }
  }

  public passwordView: string;
  private realPass: string;
  public showPass: boolean;
  public hasIcon: boolean;
  public hasMessage: boolean;
  public error_message: string;
  public hasPlaceHolder: boolean;
  public placeHolder: string;
  public isDisabled : boolean;
  private delayCopy: number;
  private delayKey: number;

  constructor() {
    this.realPass = '';
    this.showPass = false;
    this.hasIcon = false;
    this.hasMessage = false;
    this.error_message = '';
    this.hasPlaceHolder = true;
    this.placeHolder = 'Password';
    this.isDisabled = false;
    this.delayCopy = 200;
    this.delayKey = 100;
  }

  onKey(event: any) {
    if (!this.showPass) {
      if (this.stringContainsOne(event.code, ['Shift'])) {
        let posIni = Math.max(event.target.selectionStart - 1, 0);
        let posFin = event.target.value.length - (posIni + 1);
        let key = this.passwordView.substr(posIni, posIni + 1);
        if (key != '•') {
          this.realPass = this.realPass.substr(0, posIni) + key + this.realPass.substr(this.realPass.length - posFin);
          this.passwordChange.emit(this.realPass);
          setTimeout(() => {
            this.passwordView = this.passwordView.substr(0, posIni) + '•' + this.passwordView.substr(posIni + 1);
          }, this.delayKey);
        }
      } else if (this.stringContainsOne(event.code, ['Key', 'Digit', 'Numpad', 'Comma', 'Period', 'Slash'])) {
        let posIni = Math.max(event.target.selectionStart - 1, 0);
        let posFin = event.target.value.length - (posIni + 1);
        let key = this.passwordView.substr(posIni, posIni + 1);
        if (key === event.key || (key !== '•' && key.toUpperCase() !== event.key.toUpperCase())) {
          this.realPass = this.realPass.substr(0, posIni) + event.key + this.realPass.substr(this.realPass.length - posFin);
          this.passwordChange.emit(this.realPass);
          setTimeout(() => {
            this.passwordView = '•'.repeat(posIni + 1) + this.passwordView.substr(posIni + 1);
          }, this.delayKey);
        }
      } else if (event.code.search('Backspace') == 0) {
        let posIni = event.target.selectionStart - 1;
        let posFin = event.target.value.length - (posIni + 1);
        this.realPass = this.realPass.substr(0, posIni) + this.realPass.substr(this.realPass.length - posFin);
        this.passwordChange.emit(this.realPass);
      } else if (event.code.search('Delete') == 0) {
        let posIni = event.target.selectionStart;
        let posFin = event.target.value.length - (posIni + 1);
        this.realPass = this.realPass.substr(0, posIni) + this.realPass.substr(this.realPass.length - posFin);
        this.passwordChange.emit(this.realPass);
      } else {
        this.passwordView = '•'.repeat(this.realPass.length);
      }
    } else {
      if (!this.stringContainsOne(event.code, ['Shift', 'Delete', 'Backspace', 'Key', 'Digit', 'Numpad', 'Comma', 'Period', 'Slash'])) {
        this.passwordView = this.realPass;
      } else {
        this.realPass = this.passwordView;
        this.passwordChange.emit(this.realPass);
      }
    }


  }

  stringContainsOne(cadena: string, substrings: Array<string>): boolean {
    let i = 0;
    let salida = false;
    while (!salida && i < substrings.length) {
      salida = cadena.search(substrings[i]) == 0;
      i = i + 1;
    }
    return salida;
  }

  toggleShowPassword() {
    if (!this.disabled) {
        this.showPass = !this.showPass;
      if (this.showPass) {
        this.passwordView = this.realPass;
      } else {
        this.passwordView = '•'.repeat(this.realPass.length);
      }
    }
  }

  ngOnInit(){
    if (this.options){
      if (this.options.hasOwnProperty('icon')) {
        this.hasIcon = this.options.icon;
      }
  
      if (this.options.hasOwnProperty('errorMessage')) {
        this.error_message = this.options.errorMessage;
      }
  
      if (this.options.hasOwnProperty('withPlaceHolder')) {
        this.hasPlaceHolder = this.options.withPlaceHolder;
      }
  
      if (this.options.hasOwnProperty('placeHolder')) {
        this.placeHolder = this.options.placeHolder;
      }
    }
  }


}
