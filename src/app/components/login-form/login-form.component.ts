import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  public passReal : string;
  public correo: string;
  public optionsPass = {
    'icon' : true,
    'placeHolder' : 'Contraseña'
  }
  public message: string;
  public disablingInputs: boolean;

  constructor(private authServ: LoginService,
    private router: Router) { 
    this.message = '';
    this.disablingInputs = false;
  }

  ngOnInit() {
  }

  login(){
    this.disablingInputs = true;
    if ( this.validateInitialForm()) {
      /// Pedimos el servicio
      setTimeout(()=>{
        this.authServ.login(this.correo,this.passReal).subscribe((res)=>{
          this.disablingInputs = false;
          if (res){
            this.router.navigate(['/main']);
          }
        }, (error)=>{
          this.disablingInputs = false;
          console.log(error);
          
        })
      }, 3000)
    }else {
      this.disablingInputs = false;
    }

  }

  validateInitialForm() : boolean{
    let email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    this.message = '';
    let pass= true;
    if (!this.correo || this.correo.length == 0) {
      this.message = "Debes escribir un correo.\n";
      pass = false;
    } else if (!email.test(this.correo)) {
      this.message = "El correo tiene un formato incorrecto.\n";
      pass = false;
    }

    if (!this.passReal || this.passReal.length == 0){
      this.message += 'Debes escribir una contraseña.';
      pass = false;
    } 
    return pass;
  }

}
