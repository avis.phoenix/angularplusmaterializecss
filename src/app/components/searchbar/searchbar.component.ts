import { Component, AfterViewInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

/// Instancia de Materialize CSS
declare var M:any;

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements AfterViewInit {
  @Input() placeHolder: string;
  @Input() options: any;
  @Output() selectValue = new EventEmitter<string>();

  @ViewChild('autoinput', {static: false}) elem: ElementRef;

  private instanceAutoComplete : any;
  public textValue: string;
  public active: boolean;

  constructor() {
    this.active = false;
  }

  ngAfterViewInit() {
    this.instanceAutoComplete = M.Autocomplete.init(this.elem.nativeElement, 
      {
        data: this.options,
        onAutocomplete: this.selectOption
      });
  }

  changeClass(){
    if (!this.active) {
      this.active = true;
    }
  }

  closeClass() {
    this.active = false;
    this.textValue = '';
  }

  selectOption(el: any) {
    console.log(el);
    
  }

}
