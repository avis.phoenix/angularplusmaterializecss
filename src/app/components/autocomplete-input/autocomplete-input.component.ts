import { Component, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';

declare var M;

@Component({
  selector: 'app-autocomplete-input',
  templateUrl: './autocomplete-input.component.html',
  styleUrls: ['./autocomplete-input.component.css']
})
export class AutocompleteInputComponent implements AfterViewInit {

  @Input() iconMD: string;
  @Input() placeHolder: string;
  @Input() options: any;

  @ViewChild('autoinput', {static: false}) elem: ElementRef;

  private instance;

  constructor() { }

  ngAfterViewInit() {
    if (!this.instance) {
      this.instance = M.Autocomplete.init(this.elem.nativeElement, { data: this.options});
    }
    
  }

}
