import { Injectable, EventEmitter } from '@angular/core';
import { User } from '../interfaces/user.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private credentials: User;
  private loginProcess: Observable<boolean>;
  public userLogin: EventEmitter<User>;

  constructor() {
    this.credentials = null;
    this.loginProcess = null;
    this.userLogin = new EventEmitter<User>();
  }

  isLogin() {
    if (sessionStorage.getItem('anguser')){
      this.credentials = JSON.parse(sessionStorage.getItem('anguser'));
    }
    return this.credentials != null;
  }

  logout() {
    this.credentials = null;
    this.loginProcess = null;
    this.userLogin.emit(this.credentials);
    sessionStorage.removeItem('anguser');
  }

  login(username: String, password: String): Observable<boolean> {
    console.log("Verifico logueo previo");
    if ( this.loginProcess === null ) {
      this.loginProcess = new Observable<boolean>( subscriber => {
        //console.log("Empiezo a loguear");
        /// DUMMY LOGIN
        this.credentials = { 
          rol: "user",
          token: "12345678910",
          username: "Gustavo García",
          usermail: "user@mail.com",
          language: 'es',
          lastLogin: new Date("October 13, 2014 11:13:00")
        };
        sessionStorage.setItem('anguser', JSON.stringify(this.credentials));
        subscriber.next(true);
        subscriber.complete();
        this.userLogin.emit(this.credentials);
        this.loginProcess = null;
      });
      return this.loginProcess;
    }
    else {
      return null;
    }
  }
  
}
