import { Component, AfterViewInit } from '@angular/core';

declare var M;

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit() {
    let elems = document.querySelectorAll('select');
    M.FormSelect.init(elems,{ classes: 'select-small'});
  }

}
