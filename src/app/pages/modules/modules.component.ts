import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  public theMenus = [{
    title: 'Titulo 1',
    modules: [{
      name: 'titulo1',
      insumos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      procesos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      salidas: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      reportes: [{ name: 'Link 1', link: ['/views/table','L1'] }]
    }, {
      name: 'titulo2',
      insumos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      procesos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      salidas: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      reportes: [{ name: 'Link 1', link: ['/views/table','L1'] }]
    }]
  }, {
    title: 'Titulo 2',
    modules: [{
      name: 'titulo1',
      insumos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      procesos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      salidas: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      reportes: [{ name: 'Link 1', link: ['/views/table','L1'] }]
    }, {
      name: 'titulo2',
      insumos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      procesos: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      salidas: [{ name: 'Link 1', link: ['/views/table','L1'] }, { name: 'Link 2', link: ['/views/table','L2'] }],
      reportes: [{ name: 'Link 1', link: ['/views/table','L1'] },{ name: 'Link 2', link: ['/views/table','L2'] }]
    }]
  }];

  public currentID: string;

  constructor() {
    this.currentID = '';
  }

  ngOnInit() {
  }

  changeCurrent(id){
    this.currentID = id;
  }

}
