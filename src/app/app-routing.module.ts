import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { PagenotfoundComponent } from './pages/pagenotfound/pagenotfound.component';
import { ModulesComponent } from './pages/modules/modules.component';
import { TableComponent } from './pages/table/table.component';
import { ConfigSiteComponent } from './pages/config-site/config-site.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'main', component: ModulesComponent, canActivate: [ AuthGuard ], },
  { path: 'views/table/:name', component: TableComponent, canActivate: [ AuthGuard ] },
  { path: 'setup/site', component: ConfigSiteComponent, canActivate: [ AuthGuard ] },
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
