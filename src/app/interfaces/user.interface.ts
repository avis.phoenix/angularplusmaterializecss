export interface User {
    rol: string,
    token: string,
    username: string,
    usermail: string,
    language: string,
    lastLogin: Date
}

export interface UserDisplay {
    rol: string,
    username: string,
    language: string,
    lastLogin: Date
}